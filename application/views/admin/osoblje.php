<!-- autor: Milica Stanković 2009/0459 -->

<?php
/**
 * Created by PhpStorm.
 * User: Milica
 * Date: 24.5.2015.
 * Time: 20:33
 */

$this->load->library('table'); // pomoc kod kreiranja tabele
$this->load->library('session');
$CI =& get_instance(); // dohvatimo instancu codeignitera (jer sledece linije ne rade preko $this)
$CI->table->set_heading('ID','Korisnik', 'Tip','Email', 'Blokiran', 'Promeni', 'Briši'); // postavimo heading
$usertype = $CI->session->userdata('typestring');

foreach ($staff as $u) { // PAZI: radi sa redovima iz baze
    if ($u->getType()==3) //Moderator
        $CI->table->add_row(
            $u->getKID(),
            $u->getUsername(),
            ($u->getType())==3 ? "Moderator" : "Administrator",
            $u->getEmail(),
            ($u->getBlocked())? "blokiran" : "",
            ($u->getBlocked())? anchor($usertype . '/odblokiraj_moda/'.$u->getKID(), 'dozvoli') :
            anchor($usertype . '/blokiraj_moda/'.$u->getKID(), '▬'),
            anchor($usertype . '/obrisi_korisnika/'.$u->getKID(), 'X')
        );
    if ($u->getType()==4) //Administrator
        $CI->table->add_row(
            $u->getKID(),
            $u->getUsername(),
            ($u->getType())==3 ? "Moderator" : "Administrator",
            $u->getEmail()
        );
}
echo $CI->table->generate(); // napravimo tabelu
echo $CI->pagination->create_links(); // napravimo linkove za paginaciju

// TODO: odluka. Da li da "obrisane" ljude samo blokiramo? Da li zaista odobriti trajno nereverzibilno brisanje iz baze?
// ideja: moderatori ne mogu da brišu korisnike, administrator može.

?>

</br><div class=system>1) nije moguće blokirati administratora.</div>
<div class=system>2) nije moguće obrisati nalog administratora.</div>

