<!-- autor: Milica Stanković 2009/0459 -->



<div class="menu" name="usermenu" title="Korisnički meni">
<?php
$this->load->helper('url'); 
$this->load->library('session');
$CI =& get_instance();
$tip = $CI->session->userdata('typestring');
?>
    <ul class='dropdown'>
        <li id="top"><a href=<?php echo site_url($tip.'/index'); ?>>User panel</a>
            <span></span>
            <ul class="dropdown-box">

                <li><a href=<?php echo site_url($tip.'/javni'); ?>>Javni treninzi</a></li>
                <li><a href=<?php echo site_url($tip.'/moji'); ?>>Moji treninzi</a></li>
				<li><a href=<?php echo site_url($tip.'/novi'); ?>>Novi trening</a></li>
                <li><a href=<?php echo site_url('logout/logout'); ?>>Odjava</a></li>
            </ul>
        </li>
    </ul>
</div>