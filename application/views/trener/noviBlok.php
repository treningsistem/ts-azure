<!--
/**
 * Created by PhpStorm.
 * User: Milica
 * Date: 19.5.2015.
 * Time: 1:41
 */ -->

<?php
$this->load->helper('form_helper'); // treba nam form helper
$this->load->library('table'); // pomoc kod kreiranja tabele

echo form_open("trener/dodaj_blok", array("method"=>"post")); // otvorimo formu, tip je post
$CI =& get_instance(); // dohvatimo instancu codeignitera (jer sledece linije ne rade preko $this)
$CI->table->add_row(form_label("Naziv bloka:", ""), form_input('title', ''));
$CI->table->add_row(form_label("Opis:", ""), form_textarea('description', ''));
$CI->table->add_row(form_label("Trajanje:", ""), form_input('duration', '[minuti]'));
$CI->table->add_row(form_label("BLOCKNUM:", ""), form_input('blockNum', ''));
/* TODO: šta je blocknum? */
/* TODO: dodavanje bloka u neki postojeći trening */

$CI->table->add_row( form_submit('', 'Napravi')); // submit dugme

echo $CI->table->generate(); // napravimo tabelu

echo form_close(); // zatvorimo formu

/* TODO: ovde uputiti na odgovarajuću akciju za kreiranje novog bloka!*/

?>