<!-- autor: Milica Stanković 2009/0459 -->
<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
include_once('/../../models/entity/trainingtypes.php');

$this->load->library('table'); // pomoc kod kreiranja tabele
$this->load->library('session');
$CI =& get_instance(); // dohvatimo instancu codeignitera (jer sledece linije ne rade preko $this)
$usertype = $CI->session->userdata('typestring');
$CI->table->set_heading('Naslov', 'Datum', 'Opis', 'Tip', 'Obriši'); // postavimo heading
foreach ($trainings as $tr) {
	$CI->table->add_row(
		anchor($usertype . '/pregledaj/'.$tr->getTID(), $tr->getTitle()),
		$tr->getCreated(),
        strlen($tr->getDescription()) > 30 ? substr($tr->getDescription(), 0, 30)."[...]" : $tr->getDescription(),
		$tr->getType() == TrainingTypes::PublicTraining ? "Javni" : "Privatni",
		anchor($usertype . '/obrisi/'.$tr->getTID(), 'X')
	);
}
echo $CI->table->generate(); // napravimo tabelu
echo $CI->pagination->create_links(); // napravimo linkove za paginaciju

/**
 * Created by PhpStorm.
 * User: Milica
 * Date: 21.5.2015.
 * Time: 12:07
 */

?>