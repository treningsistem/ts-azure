<!-- autor: Milica Stanković 2009/0459 -->


<?php

$this->load->library('table'); // pomoc kod kreiranja tabele
$this->load->library('session');
$CI =& get_instance(); // dohvatimo instancu codeignitera (jer sledece linije ne rade preko $this)
$CI->table->set_heading('Naslov', 'Autor', 'Datum', 'Opis'); // postavimo heading
foreach ($trainings as $tr) { // PAZI: radi sa redovima iz baze
	$CI->table->add_row(
		anchor($CI->session->userdata('typestring') . '/pregledaj/'.$tr->TID, $tr->Title),
		$tr->Username,
		$tr->Created,
        strlen($tr->Description) > 50 ? substr($tr->Description, 0, 50)."[...]" : $tr->Description
	);
}
echo $CI->table->generate(); // napravimo tabelu
echo $CI->pagination->create_links(); // napravimo linkove za paginaciju

/**
 * Created by PhpStorm.
 * User: Milica
 * Date: 21.5.2015.
 * Time: 1:33
 */

?>