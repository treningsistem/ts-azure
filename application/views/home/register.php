<!-- autor: Milica Stanković 2009/0459 -->
<!-- autor: Stefan Ranković, 2014/3155 -->




<h1>Novi korisnik?</h1>
<h2>Registrujte se ovde!</h2>

<?php
$this->load->helper('form_helper'); // treba nam form helper
$this->load->library('table'); // pomoc kod kreiranja tabele
$this->load->library('form_validation');

echo form_open("register/validation"); // otvorimo formu, tip je post
$CI =& get_instance(); // dohvatimo instancu codeignitera (jer sledece linije ne rade preko $this)
$CI->table->add_row(form_label("Korisničko ime:", "username"), form_input('username', set_value('username')));

$CI->table->add_row(form_label("Email:", "email"), form_input('email', set_value('email')));
$CI->table->add_row(form_label("Lozinka:", "password1"), form_password('password1', ''));

$CI->table->add_row(form_label("Ponovi lozinku:", "password2"), form_password('password2', ''));
$CI->table->add_row(form_submit('novikorisnik', 'Registruj se'));
echo $CI->table->generate(); // napravimo tabelu
echo form_close(); // zatvorimo formu

echo validation_errors();

?>