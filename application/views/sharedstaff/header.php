<!-- autor: Milica Stanković 2009/0459 -->


<!--
 * Created by PhpStorm.
 * User: Milica
 * Date: 16.5.2015.
 * Time: 23:55
 */-->
<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title><?php echo $title;?></title>

    <!-- CSS-->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/default.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/globalMenu_style.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/dragdrop_style_preview.css" type="text/css">
    <!-- <link rel="stylesheet" href="public/css/default" type="text/css"> */-->

 </head>

 <body style="background-image:url(<?php echo base_url();?>public/images/background-admin-01.png)">

     <!-- start: Wrap-->
    <div id="wrap" style=" border-color: red" float="left" >

        <!-- start: Header-->
        <div id="header" style=" float:right;" width="99%">
            <!-- HEADER of HEADER-->
            <div id="socialstrip" style=" float:right; position: relative">
               <a href="https://www.facebook.com/"><img src="<?php echo base_url();?>public/images/facebook.png" width="36" height="36"></a>
                <a href="https://www.linkedin.com/"><img src="<?php echo base_url();?>public/images/linkedin.png" width="36" height="36"></a>
                <a href="https://plus.google.com/"><img src="<?php echo base_url();?>public/images/google.png" width="36" height="36"></a>
                <a href="https://twitter.com/"><img src="<?php echo base_url();?>public/images/twitter.png" width="36" height="36"></a>
            </div>

			<?php $this->load->view('home/userStrip'); // ako korisnik nije ulogovan, ne prikazuje nista ?>

        </div>



