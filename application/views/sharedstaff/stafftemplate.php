<!-- autor: Milica Stanković 2009/0459 -->
<!-- autor: Stefan Ranković, 2014/3155 -->



<?php
$this->load->view('sharedstaff/header', array('title'=>'STAFF | ' . $title));
$this->load->view('menu/staffMenu');
?>

<!-- start: Content-->
<div id="content" name="myContent" title="Sadrzaj" float="right" >

<?php
$this->load->view($body);
?>

<!-- end: Content-->
	<div class="push"></div>
</div>

<?php
$this->load->view('footer');
?>
