<!-- autor: Milica Stanković 2009/0459 -->


    <link rel="stylesheet" href="<?php echo base_url();?>/public/css/dragdrop_style_editor.css" type="text/css" media="screen"/>
    <!--<script type="text/javascript" src="<?php echo base_url();?>/public/javascript/dragdrop_header.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url();?>/public/javascript/redips-drag-min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/javascript/dragdrop_script.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/public/javascript/editor_essentials.js"></script>

<!-- tables inside this DIV could contain drag-able content  -->
<div id="redips-drag">

    <!-- PHP SEGMENT: TRAINING HEADER -->

    <?php
    // php parametri za punjenje tabele - info o treningu

    $trening_naslov = $training->getTitle();
    $trening_opis =  $training->getDescription();
    $trening_kreiran = $training->getCreated();
    /*$trening_autor = $training->getAuthor();*/
    // TODO: funkcija za dohvatanje imena autora

	/* SVE U TABELI KOJA SLEDI JE DEO FORME */
	$this->load->library('session');
	$this->load->helper('form_helper');
	$CI =& get_instance();
	echo form_open($CI->session->userdata('typestring') . '/snimi', array("method"=>"post")); // forma za izmenu treninga, pocinje ovde
    ?>

    <table id="tbl1">
        <colgroup>
            <col width="30"/>
            <col width="100"/>
        </colgroup>
        <tbody>
        <tr>
            <th colspan="4" class="redips-mark">
                <input class="naslov polje" type="text" name="trnaslov" placeholder="Unesite naslov treninga" value="<?php echo $trening_naslov;?>">
            </th>
        </tr>
        <tr>
            <th class="redips-mark"></th>
            <th class="redips-mark">Blok</th>
            <th class="redips-mark">Opis</th>
            <th class="redips-mark">Trajanje</th>
        </tr>
		
        <tr>
			<td class="redips-mark" />
			<td class="redips-mark"><button id="spawner" type="button" onclick="noviBlok()">Novi blok</button></td>
			<td colspan=2 class="redips-mark ukupnovreme">Ukupno trajanje: 
        
		<?php
		$t=$training->totalDuration();
		$hr=floor($t/60);
		$min=round($t-$hr*60,0);
		echo '<span id="hrs">', $hr, '</span> sati, <span id="mins">', $min, '</span> minuta.';
        ?>
		
		</td></tr>
        <tr><td colspan="4" class="redips-mark"><textarea class="tropis polje" name="tropis" placeholder="Ovde se unosi opis treninga"><?php echo $trening_opis; ?></textarea></td></tr>
		
		<?php
        for ($i=0; $i < $training->countBlocks(); $i++) {
			$block = $training->getBlock($i);
            $naslov = $block->getTitle();
            $opis = $block->getDescription();
            $trajanje = $block->getDuration();
			$blockNum = $block->getblockNum();
			
			echo '<tr class="rl">';
			echo '<td class="redips-rowhandler"><div class="redips-drag redips-row"></div></td><td class="cdark"><div class="blue">';
			echo '<input class="polje" type="text" name="naziv' . $blockNum . '" value="'. $naslov .'"></div></td>';
			echo '<td class="opis">';
			echo '<textarea class="polje"  type="" name="opis' . $blockNum . '" cols="10" >'. $opis .'</textarea></td>';
			echo '<td class="trajanje"><input class="polje" type="text" name="trajanje' . $blockNum . '" value="'. $trajanje .'" onkeypress="return isNumber(event)" onblur="calcTime()"></td>';
			echo '</tr>';
		}
		?>
		
		</tbody></table>
		
		

    <table> <!-- Kraj tabele tbl1 -->
	
	<!-- redpis kanta -->
	<colgroup><col width="100"/></colgroup><tbody><tr>
    <td class="redips-trash">Kanta</td></tr></tbody></table>
		
	<?php
        echo form_submit(array('value' => 'Sacuvaj', 'onclick' => 'save()')); // samo sadrzi jedno dugme
		if ($CI->session->userdata('type') == UserTypes::Trener) { // treneri mogu da biraju i tip treninga
			echo form_dropdown('trtip', array(TrainingTypes::PrivateTraining => 'Privatni', TrainingTypes::PublicTraining => 'Javni'), $training->getType());
		}
        echo form_close();
    ?>
</div> <!-- Kraj redpis-drag -->






