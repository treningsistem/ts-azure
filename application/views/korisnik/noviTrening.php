<!-- autor: Stefan Ranković, 2014/3155 -->

<!--
/**
 * Created by PhpStorm.
 * User: Milica
 * Date: 19.5.2015.
 * Time: 1:41
 */ -->

<?php
$this->load->helper('form_helper'); // treba nam form helper
$this->load->library('table'); // pomoc kod kreiranja tabele

echo form_open("korisnik/kreiraj", array("method"=>"post")); // otvorimo formu, tip je post
$CI =& get_instance(); // dohvatimo instancu codeignitera (jer sledece linije ne rade preko $this)
$CI->table->add_row(form_label("Naziv treninga:", ""), form_input('title', ''));
$CI->table->add_row(form_label("Opis:", ""), form_textarea('description', ''));
$CI->table->add_row(form_hidden('type', 2));
$CI->table->add_row( form_submit('', 'Napravi')); // submit dugme

echo $CI->table->generate(); // napravimo tabelu


echo form_close(); // zatvorimo formu

/* TODO: ovde uputiti na odgovarajuću akciju za kreiranje novog treninga!*/

?>