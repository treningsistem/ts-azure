<!-- autor: Stefan Ranković, 2014/3155 -->
<?php

include_once('/../mappers/usermapper.php');
include_once('/../entity/user.php');

class MTest extends CI_Model {
	public function __construct() {
        parent::__construct();
    }
	
	public function test() {
		$dbm = new UserMapper();
		$user1 = $dbm->find('Pera');
		echo $user1;
		try {
			$dbm->store($user1);
		} catch (Exception $e) {
			echo $e->getMessage();
		}
		$user2 = new User();
		$user2->setUsername("Neko");
		$user2->setPassword("Sifra");
		$user2->setEmail("neki@mail.com");
		$user2->setBlocked(0);
		$user2->setType(1);
		try {
			$dbm->store($user2);
		} catch (Exception $e) {
			echo $e->getMessage();
		}
		$users = $dbm->findAll(0, 10);
		foreach ($users as $u) {
			echo $u;
		}
		$user2->setPassword("NovaSifra");
		$dbm->update($user2);
		echo "   ";
		$admin = $dbm->find("admin");
		if (password_verify("admin", $admin->getPassword())) {
			echo "success! sifre su iste!";
		}
		$users = $dbm->findAllType(0, 10, 1);
		foreach ($users as $u) {
			echo $u;
		}
		$total = $dbm->countAll();
		echo $total;
		$total = $dbm->countAllType(1);
		echo $total;
	}
}
?>
