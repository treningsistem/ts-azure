<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
include_once('block.php');
include_once('trainingtypes.php');

class Training {
	private $TID;
	private $title;
	private $description;
	private $created;
	private $type;
	private $ownerID;
	private $blocks; // ovde su smesteni blokovi za ovaj trening
	
	public function __construct() { 
		$this->blocks = array(); // inicijalizujemo niz blokova
		$this->created = date("Y-m-d H:i:s"); // da se korisnici ne bi cimali
		$this->title = "DEFAULT TITLE"; // postavljaju se neke default vrednosti
		$this->description = "DEFAULT DESCRIPTION";
		$this->type = TrainingTypes::PrivateTraining;
	} 
	
	// get i set za TID
	public function getTID() { return $this->TID; }
	public function setTID($TID) { $this->TID = $TID; }
	
	// get i set za naslov
	public function getTitle() { return $this->title; }
	public function setTitle($title) { $this->title = $title; }
	
	// get i set za description
	public function getDescription() { return $this->description; }
	public function setDescription($description) { $this->description = $description; }
	
	// get i set za created
	public function getCreated() { return $this->created; }
	public function setCreated($created) { $this->created = $created; }
	
	// get i set za type
	public function getType() { return $this->type; }
	public function setType($type) { 
		if ($type == TrainingTypes::PrivateTraining || $type == TrainingTypes::PublicTraining) { // proverimo na ispravnost parametara
			$this->type = $type; 
		}
	}
	
	// get i set za ownerID
	public function getOwnerID() { return $this->ownerID; }
	public function setOwnerID($ownerID) { $this->ownerID = $ownerID; }
	
	// get i set za blocks array
	public function getBlocks() { return $this->blocks; }
	public function setBlocks($blocks) { $this->blocks = $blocks; }
	
	public function __toString()
    {
        $result = $this->TID . " " . $this->title . " " . $this->description . " " . $this->created . " " . $this->type . " " . $this->ownerID . " -BLOKOVI - ";
		foreach($this->blocks as $block) {
			$result .= $block . " ";
		}
		return $result;
    }
	
	public function append($block) { // dodaje blok na poziciju $blockNum i updejtuje blockNum za sve blokove koji dolaze posle njega
		$left = array_slice($this->blocks, 0, $block->getBlockNum()); // levi deo niza bice prvih blockNum blokova
		$right = array_slice($this->blocks, $block->getBlockNum()); // desni deo niza bice sve od blockNum pa do kraja
		$this->blocks = array_unique(array_merge((array)$left, array($block), (array)$right)); // onda sve ukombinujemo sa novim blokom u sredini
	}
	
	public function remove($blockNum) { // isto kao gore samo sto brise
        // umesto block → blocks?
		$left = array_slice($this->blocks, 0, $block->getBlockNum()); // levi deo niza bice prvih blockNum blokova
		$right = array_slice($this->blocks, $block->getBlockNum()+1); // desni deo niza bice sve od blockNum pa do kraja, ali bez blockNum bloka
		$this->blocks = array_unique(array_merge((array)$left, (array)$right)); // onda ukombinujemo levu i desnu polovinu
	}
	
	public function countBlocks() { // vraca broj blokova
		return count($this->blocks);
	}
	
	public function getBlock($num) { // dohvata blok koji je $num po redu
		foreach($this->blocks as $b) {
			if ($b->getBlockNum() == $num) {
				return $b;
			}
		}
		return null; // ne postoji trazeni blok
	}
	
	public function getBlockByNumber($blockNum) { // dohvata blok po rednom broju
		return $this->blocks[$blockNum];
	}
	
	public function totalDuration() {
		$total = 0;
		foreach($this->blocks as $b) {
			$total += $b->getDuration();
		}
		return $total; // vraca trajanje treninga, u minutima
	}
}
?>
