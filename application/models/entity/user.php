<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
class User {
	private $KID; // korisnicki ID
	private $username; // treba da bude unique u sistemu
	private $password; // cuva se kao hash (string od 60 karaktera)
	private $email;
	private $blocked;
	private $type;
	
	public function __construct() {}
	
	// get i set za KID
	public function setKID($KID) {
		$this->KID = $KID;
	}
	public function getKID() {
		return $this->KID;
	}
	
	// get i set za username
	public function setUsername($username) {
		$this->username = $username;
	}
	public function getUsername() {
		return $this->username;
	}
	
	// get i set za password
	public function setPassword($password) {
		$this->password = $password;
	}
	public function getPassword() {
		return $this->password;
	}
	
	// get i set za email
	public function setEmail($email) {
		$this->email = $email;
	}
	public function getEmail() {
		return $this->email;
	}
	
	// get i set za blocked
	public function setBlocked($blocked) {
		$this->blocked = $blocked;
	}
	public function getBlocked() {
		return $this->blocked;
	}
	
	// get i set za type
	public function setType($type) {
		$this->type = $type;
	}
	public function getType() {
		return $this->type;
	}
	
	// kao u javi
	public function __toString()
    {
        return $this->username . " " . $this->password . " " . $this->email . " " . $this->blocked . " " . $this->type;
    }
}
?>
