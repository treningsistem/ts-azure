<!-- autor: Milica Stanković 2009/0459 -->
<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
include_once('/mappers/trainingmapper.php');
include_once('/mappers/usermapper.php');
include_once('/entity/training.php');
include_once('/entity/block.php');

class UserActions extends CI_Model {
	private $tmap; // trening mapper
    private $umap; // user mapper
	
	public function __construct() {
		$this->tmap = new TrainingMapper(); // samo postavi training mapper
        $this->umap = new UserMapper(); // samo postavi training mapper
		$this->load->library('session'); // cuvace sve korisnikove treninge i tekuci trening na kome korisnik radi
	}

    // TRAINING MANAGEMENT

    public function loadMyTrainings($KID) {
        if (!empty($KID)) { // samo ako je dati $KID validan
            $this->session->set_userdata(array('myTrainings' => $this->tmap->findByKID($KID)));
            // pokupi sve treninge za korisnika KID iz baze i CUVA U SESIJI
        }
    }

    public function preview($TID) { // korisnik trazi trening koji zeli da pregleda
        $tr = $this->tmap->findByTID($TID); // vraca trening sa ID TID
        if (!empty($tr) && ($tr->getType() == TrainingTypes::PublicTraining || $tr->getOwnerID() == $this->session->userdata('KID'))) { // samo ako je javni ili moji vratice ga
            return $tr;
        }
        return null;
    }

    public function getMyTrainings() {
        return $this->session->userdata('myTrainings'); // vraca moje treninge
    }

    public function getSomePublicTrainings($offset, $limit) { // vraca nekoliko treninga
        return $this->tmap->findSomePublic($offset, $limit);
    }
	public function getSomeTrainings($offset, $limit) {
        return $this->tmap->findSomeTrainings($offset, $limit); // vraca nekoliko treninge
    }

    public function countPublicTrainings() { // vraca broj javnih treninga
        return $this->tmap->countPublicTrainings();
    }
	
	public function countAllTrainings() { // vraca broj javnih treninga
        return $this->tmap->countAllTrainings();
    }

    private function getTrainingByTID($TID) { // dohvata trening po TID
        foreach ($this->session->userdata('myTrainings') as $tr) {
            if ($tr->getTID() == $TID) { // ako se poklapaju TID treninga i trazeni TID
                return $tr; // vraca trening
            }
        }
        return null; // u suprotnom, null
    }

    public function isMyTraining($TID) { // vraca true ako trening pripada ovom korisniku, u suprotnom false
        return $this->getTrainingByTID($TID) != null ? true : false; // ako ne dobije null, znaci da je nas trening, u suprotnom nije
    }

    public function newTraining() { // ova funkcija kreira novi objekat treninga na kome se radi. Poziva se kada se ide na "Napravi novi trening" stranicu
        $currentTraining = new Training();
        $currentTraining->setOwnerID($this->session->userdata('KID')); // ovo moze ovde da se uradi jer korisnici ne mogu menjati vlasnistvo nad treningom
        $this->session->set_userdata(array('currentTraining' => $currentTraining)); // samo napravi novi objekat
    }

    public function loadTraining($TID) { // ova funkcija se poziva kada se ide na stranicu za izmenu postojecih treninga. Postavlja postojeci trening za currentTraining
        $this->session->set_userdata(array('currentTraining' => $this->getTrainingByTID($TID)));
    }

    public function createTraining($title, $description, $type) { // napravi i cuva trening
        $this->session->userdata('currentTraining')->setTitle($title);
        $this->session->userdata('currentTraining')->setDescription($description);
        $this->session->userdata('currentTraining')->setType($type);
        $this->trainingmapper->store($this->session->userdata('currentTraining')); // sacuvamo u bazi (setovace TID)
        $this->session->userdata('myTrainings')[] = $this->session->userdata('currentTraining'); // dodaje u listu treninga
    }

    public function update($title, $description, $type) { // cuva izmene vec postojeceg treninga
        $this->session->userdata('currentTraining')->setTitle($title);
        $this->session->userdata('currentTraining')->setDescription($description);
        $this->session->userdata('currentTraining')->setType($type);
        $this->tmap->update($this->session->userdata('currentTraining')); // updejtujemo ga u bazi
    }

    // BLOCK MANAGEMENT

    public function save($currentTraining) {
        $this->session->set_userdata(array('currentTraining' => $currentTraining));
        if(!empty($currentTraining->getTID())) { // proverava da li current trening ima TID
            $this->tmap->update($currentTraining); // ako ima, znaci da je vec zaveden u bazi i treba uraditi update
        } else {
            $this->tmap->store($currentTraining); // ako nema, znaci da ga treba dodati kao novi red u bazi
        }
    }

    // USER MANAGEMENT

    public function getAllStaff() {
        return $this->umap->findAllStaff(); // vraca sve korisnike
    }
	
	public function getSomeUsers($offset, $limit) {
        return $this->umap->findSomeUsers($offset, $limit); // vraca sve korisnike
    }
	
	public function countAllUsers() { // vraca broj javnih treninga
        return $this->umap->countAllUsers();
    }

    public function promote_user($UID) {
        return $this->umap->promote_user($UID); // vraca sve korisnike
    }

    public function delete_user($UID) { // brise i treninge
        $this->umap->delete_user($UID);
		$trainings = $this->tmap->findByKID($UID);
		foreach ($trainings as $t) {
			$this->tmap->delete($t->getTID());
		}
    }

    public function block_user($UID) {
        $this->umap->block_user($UID);
    }

    public function unblock_user($UID) {
        $this->umap->unblock_user($UID);
    }
}

?>
