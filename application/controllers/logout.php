<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
include_once('/../models/entity/usertypes.php');

class Logout extends CI_Controller {	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
	}
	
	public function logout() {
		$userType = $this->session->userdata('type'); // dohvatimo tip
		$this->session->sess_destroy(); // unistimo sesiju
		if ($userType == UserTypes::Moderator || $userType == UserTypes::Admin) {
			redirect('start/staff', 'refresh'); // vraca moderatore i admina na login stranicu za njih
		} else {
			redirect('start/index', 'refresh'); // svi ostali idu na glavni login
		}
	}
}
?>