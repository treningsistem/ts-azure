<!-- autor: Stefan Ranković, 2014/3155 -->
<?php
class CBlockTest extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('test/blocktest');
	}

	public function test()
	{
		$this->blocktest->test();
	}
}
?>
