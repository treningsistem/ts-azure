<!-- autor: Milica Stanković 2009/0459 -->

<?php

include_once('basestaff.php');
include_once('/../models/entity/usertypes.php');


include_once('/../models/entity/usertypes.php');
include_once('/../models/entity/user.php');
include_once('/../models/entity/trainingtypes.php');
include_once('/../models/entity/training.php');
include_once('/../models/entity/block.php');

class Admin extends BaseStaff {

	public function __construct() { // kontroler koji nudi interface za korisnike
		parent::__construct();
		$userType = $this->session->userdata('type'); // dohvatimo tip logovanog korisnika
		if (!isset($userType) || $userType != UserTypes::Admin) {
			redirect('start/staff', 'refresh'); // admin nije ulogovan, vracamo ga na login
			die();
		}
	}

    public function osoblje() { // Osoblje management za admina
		$staff= $this->useractions->getAllStaff();
        $this->load->view('sharedstaff/stafftemplate', array('body' => 'admin/osoblje', 'title' => 'Osoblje', 'staff' => $staff));
    }
	
	 public function obrisi_korisnika($UID){

        $this->useractions->delete_user($UID); // briše se trening sa $TID
		
        redirect($this->session->userdata('typestring') . '/korisnici', 'refresh');
        //pogled na administraciju svih korisnika
    }
	
	
    public function blokiraj_moda($UID){
        // blokiraj korisnika sa $UID
        $this->useractions->block_user($UID); // briše se trening sa $TID

        redirect($this->session->userdata('typestring') . '/osoblje', 'refresh');
    }

    public function odblokiraj_moda($UID){
        // blokiraj korisnika sa $UID
        $this->useractions->unblock_user($UID); // briše se trening sa $TID

        redirect($this->session->userdata('typestring') . '/osoblje', 'refresh');
    }

}

?>
