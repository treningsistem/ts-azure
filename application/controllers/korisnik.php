<!-- autor: Stefan Ranković, 2014/3155 -->

<?php

include_once('baseuser.php');
include_once('/../models/entity/usertypes.php');

class Korisnik extends BaseUser {
	public function __construct() { // kontroler koji nudi interface za korisnike
		parent::__construct();
		$userType = $this->session->userdata('type'); // dohvatimo tip logovanog korisnika
		if (!isset($userType) || $userType != UserTypes::RK) {
			redirect('start/index', 'refresh'); // vracamo korisnika na login, posto nije ulogovan
			die();
		}
	}
}

?>
