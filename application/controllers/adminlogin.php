<!-- autor: Milica Stanković 2009/0459 -->

<?php
include_once('baselogin.php');

class AdminLogin extends BaseLogin {	
	public function __construct() {
		parent::__construct();
	}

	protected function performLogin($username, $password) {
		return $this->loginmodel->loginSupervisor($username, $password); // pokusamo login
	}
}
?>
