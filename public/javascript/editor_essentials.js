function noviBlok() {
	var table = document.getElementById("tbl1"); // dohvatim tbl1

	var row = table.insertRow(-1); // stavim na kraj novi red
	row.className  = "rl";

	// Insert new cells (<td> elements) of the "new" <tr> element:
	var cell0 = row.insertCell(0); // trebaju mi 4 celije
	cell0.className  = "redips-rowhandler";
	var cell1 = row.insertCell(1);
	cell1.className  = "cdark";
	var cell2 = row.insertCell(2);
	cell2.className  = "opis";
	var cell3 = row.insertCell(3);
	cell3.className  = "trajanje";

	// Sadrzaj celija su informacije o bloku
	cell0.innerHTML = '<div class="redips-drag redips-row"></div>';
	cell1.innerHTML = '<div class="blue"><input class="polje" type="text" name="nazivX" placeholder="Naziv bloka"></div>';
	cell2.innerHTML = '<textarea class="polje"  type="" name="opisX" rows="2" cols="10" placeholder="Detalji o bloku."></textarea>';
	cell3.innerHTML = '<input class="polje" type="text" name="trajanjeX" placeholder="15" onkeypress="return isNumber(event)" onblur="calcTime()">';
			
	REDIPS.drag.init(); // restartujemo handler jer u suprotnom nece raditi drag and drop za nove objekte
}
		
function save() { // prevezuje imena inputa, da bi bili po redu kao sto su blokovi naredjani (blok 0 ima polja opis0, naziv0, trajanje0 itd.)
	var table = document.getElementById("tbl1");
	var rowstart = 4; // pocinjemo od cetvrtog reda, prvih 4 su naslov, opis, dugme i deskripcioni red
	var trs = table.getElementsByTagName("tr");
	for(var i = rowstart; i < table.rows.length; i++) {
		var cell1 = trs[i].cells[1];
		var input1 = cell1.getElementsByTagName("div")[0].getElementsByTagName("input")[0]; // ovo nam treba
		input1.setAttribute("name", "naziv" + (i - rowstart)); // prepravimo ime inputa na input + i - rowstart
		
		var cell2 = trs[i].cells[2];
		var textarea2 = cell2.getElementsByTagName("textarea")[0];
		textarea2.setAttribute("name", "opis" + (i - rowstart)); // opis + i-rowstart
		
		var cell3 = trs[i].cells[3];
		var input3 = cell3.getElementsByTagName("input")[0];
		input3.setAttribute("name", "trajanje" + (i - rowstart)); // trajanje + i-rowstart
	}
}

function isNumber(evt) { // proverava da li je unet broj u duration
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function calcTime() { // azurira ukupno vreme treninga
	var vreme = 0;
	var trajanja = 	document.getElementsByClassName("trajanje"); // dohvatimo sve redove
	if (trajanja === null) return; // vracamo se ako nema redova
	for (var i=0; i < trajanja.length; i++) { // prodjemo kroz sve elemente koji predstavljaju trajanje
		var chunk = trajanja[i].getElementsByTagName("input")[0].value;
		if (chunk == "") chunk = "0"; // provera na prazan string
		vreme += parseInt(chunk); // pokupimo im vrednost
	}
	var hrs = Math.floor(vreme/60); // floorujemo
	var mins = vreme%60;
	var hrsTag = document.getElementById("hrs");
	var minsTag = document.getElementById("mins");
	hrsTag.innerHTML = hrs;
	minsTag.innerHTML = mins;
}

function redipsDeleteEvent() { // stvari koje se desavaju pri brisanju redips redova
	var tr = document.getElementsByClassName("rl"); // dohvatimo sve rl elemente
	if (tr.length == 1 && tr[0].getElementsByClassName("redips-rowhandler")[0].innerHTML == "") { // ostao je poslednji red rl tipa i prazan je (ovo je izgleda bug u redipsu)
		tr[0].parentNode.removeChild(tr[0]); // ovo radimo zato sto ako se jedini blok obrise ostaje prazan red
	}
	calcTime(); // takodje, preracunamo vreme
}

REDIPS.drag.event.rowDeleted = redipsDeleteEvent;

